NAME
  dodgy - searches for dodgy looking lines in Python code

SYNOPSIS
  dodgy
  
DESCRIPTION
  dodgy checks all modules in ./ for:

  * hard coded Amazon Web Services (AWS) secret access keys

  * hard coded passwords and secret keys

  * hard coded SSH private and public keys

  * accidental SCM diff checkins

AUTHORS
  dodgy was written by Landscape.io <code@landscape.io>
